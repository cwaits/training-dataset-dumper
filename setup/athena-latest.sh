SCRIPT_PATH=${BASH_SOURCE[0]:-${0}}
source ${SCRIPT_PATH%/*}/deconda.sh

echo "=== running setupATLAS ==="
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

# If you need to run on a nightly, see ATLINFR-4697
echo "=== running asetup ==="
asetup Athena,main,latest

# add h5ls
source ${SCRIPT_PATH%/*}/add-h5-tools.sh
source ${SCRIPT_PATH%/*}/allow-breaking-edm.sh
