#ifndef PARENT_LINK_DECORATOR_ALG_HH
#define PARENT_LINK_DECORATOR_ALG_HH

#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "GaudiKernel/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "StoreGate/WriteDecorHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"

#include "xAODJet/JetContainer.h"
#include "AthLinks/ElementLink.h"

class ParentLinkDecoratorAlg :  public AthReentrantAlgorithm { 
public:
  
  /**< Constructors */
  ParentLinkDecoratorAlg(const std::string& name, ISvcLocator *pSvcLocator);
  
  /**< Main routines */
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext&)  const override;

private:
  
  SG::ReadHandleKey< xAOD::JetContainer > m_JetContainerKey {
    this, "JetContainer", "HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_nojcalib_ftf",
      "Key for the input jet collection"};

  SG::WriteDecorHandleKey< xAOD::JetContainer  > m_new_link {
    this, "ParentLinkName", "Parent", "Link to be added to the Jet"};
};

#endif
