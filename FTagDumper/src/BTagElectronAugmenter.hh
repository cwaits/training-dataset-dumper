#ifndef BTAG_ELECTRON_AUGMENTER_HH
#define BTAG_ELECTRON_AUGMENTER_HH

#include "xAODBTagging/BTaggingContainerFwd.h"
#include "AthLinks/ElementLink.h"
#include "xAODJet/Jet.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "ElectronPhotonSelectorTools/AsgElectronSelectorTool.h"
#include "ElectronPhotonSelectorTools/ElectronSelectorHelpers.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a jet with additional information

class BTagElectronAugmenter
{
public:
  using BL = ElementLink<xAOD::BTaggingContainer>;
  using BLA = SG::AuxElement::ConstAccessor<BL>;
  BTagElectronAugmenter(const BTagElectronAugmenter::BLA& btag_link);

  // this is the function that actually does the decoration
  void augment(const xAOD::Jet& jet, const xAOD::ElectronContainer& electrons, const xAOD::Vertex& pv, 
               const EventContext& ctx, const AsgElectronSelectorTool& electron_select) const;
  void update_var(const xAOD::Jet& jet, const xAOD::ElectronContainer& electrons, const xAOD::Vertex& pv, 
                  const TLorentzVector jet_4vec, const int el_index, const std::string el_wp, const float max_phf) const;

  // void check_rc(StatusCode code); 

private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.
  // link to access the b-tagging object
  BLA m_btagging_link;

  // add 19 softe vars 
  SG::AuxElement::Decorator<char> m_dec_el_isdefault_wp3;
  // kinematic for 100 wp
  SG::AuxElement::Decorator<float> m_dec_el_pt_r_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_dr_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_abseta_wp3;
  // tracking vars 100 wp
  SG::AuxElement::Decorator<float> m_dec_el_d0_signed_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_z0_signed_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_d0_signed_sig_wp3;
  // additional var 
  SG::AuxElement::Decorator<float> m_dec_el_iso_pt_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_phf_wp3;
  // tracking dnn 100wp (x4)
  SG::AuxElement::Decorator<float> m_dec_el_epht_wp3;
  // track cluster 100wp (x3)
  SG::AuxElement::Decorator<float> m_dec_el_dphires_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_eop_wp3;
  // cal (x9) 100wp 
  SG::AuxElement::Decorator<float> m_dec_el_rhad_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_rhad1_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_eratio_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_weta2_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_reta_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_f1_wp3;
  SG::AuxElement::Decorator<float> m_dec_el_f3_wp3;
};

#endif
