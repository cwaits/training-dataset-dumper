#include "TriggerJetGetterAlg.h"
#include "AsgMessaging/MessageCheck.h"

#include "xAODJet/JetAuxContainer.h"
#include "xAODBTagging/BTaggingAuxContainer.h"

#include "xAODBTagging/BTaggingUtilities.h"

#include <memory>


TriggerJetGetterAlg::TriggerJetGetterAlg(const std::string& name,
                                         ISvcLocator* pSvcLocator):
  EL::AnaAlgorithm(name, pSvcLocator),
  m_jetLink("jetLink"),
  m_btagLink("btaggingLink")
{
}

StatusCode TriggerJetGetterAlg::initialize() {
  // get handle to TrigDecisionTool
  ATH_CHECK(m_triggerDecision.retrieve());
  ATH_CHECK(m_jetModifiers.retrieve());
  for (auto& handle: m_jetModifiers) {
    ATH_CHECK(handle.retrieve());
  }
  ATH_CHECK(m_trigJetKey.initialize());
  ATH_CHECK(m_outputJets.initialize());
  ATH_CHECK(m_outputBTag.initialize(!m_outputBTag.empty()));

  return StatusCode::SUCCESS;
}
StatusCode TriggerJetGetterAlg::execute () {

  const EventContext& context = Gaudi::Hive::currentContext();
  ATH_MSG_DEBUG("Recording jets");
  SG::WriteHandle outputJets(m_outputJets, context);
  ATH_CHECK(outputJets.record(
              std::make_unique<xAOD::JetContainer>(),
              std::make_unique<xAOD::JetAuxContainer>()));

  using BWH = SG::WriteHandle<xAOD::BTaggingContainer>;
  std::unique_ptr<BWH> outputBTags;
  if (!m_outputBTag.empty()) {
    ATH_MSG_DEBUG("Recording btagging");
    outputBTags = std::make_unique<BWH>(m_outputBTag, context);
    ATH_CHECK(outputBTags->record(
                std::make_unique<xAOD::BTaggingContainer>(),
                std::make_unique<xAOD::BTaggingAuxContainer>()));
  }

  const std::string& chain = m_bjetChain;
  if( m_triggerDecision->isPassed(chain) ){
    ATH_MSG_DEBUG("Primary chain passed " << chain);
    for (const std::string& add_chain: m_additionalChains) {
      if (!m_triggerDecision->isPassed(add_chain)) {
        ATH_MSG_DEBUG("Additional chain did not pass " << add_chain);
        return StatusCode::SUCCESS;
      } else {
        ATH_MSG_DEBUG("Additional chain passed " << add_chain);
      }
    }
  } else{
    ATH_MSG_DEBUG("Primary chain did not pass " << chain);
    setFilterPassed(false);
    return StatusCode::SUCCESS;
  }

  // logic copied from here:
  // https://gitlab.cern.ch/cnass/athena/-/blob/ARTtrigger/PhysicsAnalysis/JetTagging/FlavourTaggingTests/src/PhysicsTriggerVariablePlots.cxx#L121
  ATH_MSG_DEBUG("Getting jets with key " << m_trigJetKey.key());
  auto jets = m_triggerDecision->features<xAOD::JetContainer>(
    chain, TrigDefs::Physics, m_trigJetKey.key());
  ATH_MSG_DEBUG("Got " << jets.size() << " jets");
  for (const auto& jet_link: jets) {
    const xAOD::Jet* trig_jet = *jet_link.link;
    // TODO: can we just push trig_jet into the output vector without
    // making a new one?
    auto jet = std::make_unique<xAOD::Jet>();
    auto* jetp = jet.get();
    outputJets->push_back(std::move(jet));
    *jetp = *trig_jet;

    // Now add the b-tagging object
    if (outputBTags) {
      const xAOD::BTagging* trig_btag = xAOD::BTaggingUtilities::getBTagging(
        *trig_jet);
      BWH& btags = *outputBTags;
      *btags->emplace_back(new xAOD::BTagging()) = *trig_btag;

      // and link them together
      ElementLink<xAOD::JetContainer> jetLink(
        m_outputJets.hashedKey(),
        outputJets->back()->index(), context);
      m_jetLink(*btags->back()) = jetLink;
      ElementLink<xAOD::BTaggingContainer> btagLink(
        m_outputBTag.hashedKey(),
        btags->back()->index(), context);
      m_btagLink(*outputJets->back()) = btagLink;
    }
  }
  for (const auto& tool: m_jetModifiers) {
    ATH_CHECK(tool->modify(*outputJets));
  }
  return StatusCode::SUCCESS;
}
StatusCode TriggerJetGetterAlg::finalize () {
  return StatusCode::SUCCESS;
}
